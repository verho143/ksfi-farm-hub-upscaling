# KSFI - Farm Hub Upscaling
*[link to application](https://aeverhoek.users.earthengine.app/view/farmhubupscaling)*

## Description
This project is part of a Academic Consultancy Training Project (ACT) at Wageningen University, commissioned by Kidlinks Small Farm Incubator ([KSFI](https://ksfi.co.za/)). The Farm Hub Upscaling is a Google Earth Engine (GEE) application, where users can retrieve a regenerative farm suitability map for a given ward in South Africa. This way, KSFI, or other regenerative farm initiatives, can quickly retrieve information about any area in South Africa to upscale their programme. 
The farm suitability is calculated based on weighted layers of land use, soil type, topography (slope & aspect), proximity to water, and proximity to infrastructure (buildings & roads). 

## Usage
The application shows Buffalo City, Ward 32 (Eastern Cape, South Africa) by default. In the dropdown menu, the user can select a South African province, municipality and ward to generate the suitability map of the area.

## Project structure

### Files
* **main.js**: main source code of the project.

* **Proximities.js**: Module file, contains the function to calculate proxitmities

* **Weights.js**: Module file, contain the functions to assign weights to every layer, and the function to calculate the suitabilty.

* **DataAquisition.js**: script used to obtain DEM, calculate slope and aspect, and upload the three rasters to the ksfi-app asset.

* **Weights.xls**: excel file that contains 3 sheets explaining the weights:
    1. *Subweights per factor*: Gives the weights of the intervals of different layers. Of this sheet, the weights determined for roads, buildings, water, land use and topography are used in the application.
    2. *Weights per factor*: Gives the weight of the layers for a suitability map of Buffalo City Ward 32. The weights were established using pairwise comparison, and adapted thereafter accordingly.
    3. *Weights per factor - adapted for app*: Gives the adapted weights, based on the second sheet. The weights for vegetation and soil erosion were excluded, the other weights were recalculated to sum to 1 accordingly.

* **SoilMatrix_application.xls**: Excel file containing the reclassification and pairwise comparison of the main soil types in South Africa.
    1. *soiltypes_reclassification*: Sheet that shows the soil types in South Africa, with the codes, main soil types and sub-soil types. The soils are reclassified to the main soil types.
    2. *reasoningAndRanking*: Sheet showing the reasoning and initial ranking of the main soil types. Here, the suitability of soil is ranked for agricultural applications.
    3. *PairwiseMatrix*: The pairwise comparison matrix of the main soiltypes. This matrix is used in the application to recalculate subfacter weights for new areas with different soiltypes. 

### Data sources

The data used for this project was retrieved from different sources and uploaded in the Earth Engine cloud asset "ksfi-app". 

* **SA_DEM**: Raster of the Digital Elevation Model of South Africa, 30 M. Acquired via the Earth Engine Database: [NASADEM: NASA NASADEM Digital Elevation 30m](https://developers.google.com/earth-engine/datasets/catalog/NASA_NASADEM_HGT_001).

* **SA_LandUse**: Raster file of the Land Use map of South Africa, 2018. Acquired from the South African department of forestry, fisheries and the environment: [South African National Land-Cover (SANLC) 2018](https://www.dffe.gov.za/projectsprogrammes/egis_landcover_datasets).

* **SA_WaterLines**: Shapefile of all the the waterways in South Africa, as lines. Acquired from the Humatarian Data Exchange database, as 'hotosm_zaf_waterways_lines_shp.zip', part of the [HOTOSM South Africa Waterways (OpenStreetMap Export)](https://data.humdata.org/dataset/hotosm_zaf_waterways).


* **SA_WaterPoly**: Shapefile of all the the waterways in South Africa, as polygons. Acquired from the Humatarian Data Exchange database, as 'hotosm_zaf_waterways_polygons_shp.zip', part of the [HOTOSM South Africa Waterways (OpenStreetMap Export)](https://data.humdata.org/dataset/hotosm_zaf_waterways).

* **SA_Roads**: Shapefile of all the the roads in South Africa. Acquired from the Humatarian Data Exchange database, as 'hotosm_zaf_roads_lines_shp.zip', part of the [HOTOSM South Africa Roads (OpenStreetMap Export)](https://data.humdata.org/dataset/hotosm_zaf_roads).

* **SA_Buildings**: Shapefile of all the the buildings in South Africa. Acquired from the Humatarian Data Exchange database, as 'hotosm_zaf_buildings_polygons_shp.zip', part of the [HOTOSM South Africa Buildings (OpenStreetMap Export)](https://data.humdata.org/dataset/hotosm_zaf_buildings).

* **SA_admlvl4**: Shapefile of level 4 administrative boundaries in South Africa (wards). Acquired from the Humaritarian Data Exchange database, as zaf_adm_sadb_ocha_20201109_shp.zip, part of the [South Africa - Subnational Administrative Boundaries](https://data.humdata.org/dataset/cod-ab-zaf?).

* **SA_slope**: Rasterfile of the slope values (in degrees) of South Africa. Obtained using the [ee.Terrain.slope](https://developers.google.com/earth-engine/apidocs/ee-terrain-slope) function in GEE, using SA_DEM.

* **SA_aspect**: Rasterfile of the aspect values (in degrees, 0-360) of South Africa. Obtained using the [ee.Terrain.aspect](https://developers.google.com/earth-engine/apidocs/ee-terrain-aspect) function in GEE, using SA_DEM.

* **ZA_SOILS**: Shapefile of polygons depicting the soil types in South Africa. Part of the [ISRIC Soil and Terrain Database (SOTER) for South Africa](https://data.isric.org/geonetwork/srv/eng/catalog.search#/metadata/c3f7cfd5-1f25-4da1-bce9-cdcdd8c1a9a9) database.

## Methodology - calculation of the suitability index

The application calculates a suitability index based on weighted layers for infrastructure, roads, buildings, land use, soil type, water and topography. 
For every layer, except soil type, the values are reclassified into intervals. The weights of of the intervals are found in *Weights.xls - Subweights per factor*. Each layer is reclassified to a weighted layer.
Soil type subfactors are weighted based on pairwise comparison. Here, subfactors (main soil types) are ranked on importance based on the methodology of Saaty (1978). The matrix can be found in *SoilMatrix_application.xls - PairwiseMatrix*. This matrix is used to recalculate the weights between different soiltypes for a new area. The soil layer is then reclassified into a weighted layer accordingly.

The weighted layers of infrastructure, roads, buildings, land use, soil type, water and topography are used to calculate a final suitability map. Here, each layer is weighted as well. The initial weights were retrieved by pairwise comparison in collaboration with the commissioner. After that, the weights were adjusted accordingly. The final weights are used in this application, after they were recalculated (see *Weights.xls - Weights per factor - adapted for app*). The final suitability layer is calculted using the following formula:

`SI = 0.012 * road + 0.115 * build + 0.273 * water + 0.150 * lu + 0.221 * topo + 0.230 * soil`


## Authors and acknowledgment
Author: Annemieke Verhoek (annemieke.verhoek@wur.nl)

Acknowledgments: Much thanks to my team members Tom Winter, Mira Theilmann, Ate Jepma, Guillermo González Fradejas, and Juriaan Rijsdijk for their support and enthousiasm for this subproject. Additionally, thanks to KSFI for their enthousiasm and support during the ACT project. 


## License
MIT

## References


Saaty, R. W. (1987). The analytic hierarchy process—what it is and how it is used. Mathematical Modelling, 9(3-5), 161-176.

