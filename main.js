// ****************************************************************************************************************** //
// *************** Main script for generating the suitability map in a selected region of South Africa ************** //
// ****************************************************************************************************************** //

///// Import modules /////


var calc_proximity = require('users/aeverhoek/ACT_KSFI:Proximities');
var weights = require("users/aeverhoek/ACT_KSFI:Weights");

///// Set region in South Africa /////

// The default country in the App display is Buffalo City Ward 32

var province = ee.FeatureCollection("projects/ksfi-app/assets/SA_admlvl4")
                    .filter(ee.Filter.eq('ADM1_EN', "Eastern Cape"));
var municipality = province.filter(ee.Filter.eq("ADM3_EN", "Buffalo City"));
var ROI = municipality.filter(ee.Filter.eq("ADM4_EN", "032"));

Map.centerObject(ROI);

// https://gis.stackexchange.com/questions/451129/only-a-ui-widget-can-be-added-to-a-panel


/// Pre-process data ///

// Topography \\

// Slope
var slope = ee.Image("projects/ksfi-app/assets/SA_slope")
                .clip(ROI.geometry());
// Aspect
var aspect = ee.Image("projects/ksfi-app/assets/SA_aspect")
                .clip(ROI.geometry());
                
// Topo weighted layer
var topo_weighted = weights.topo_weight(slope, aspect).clip(ROI);

//Map.addLayer(topo_weighted)
                
// Land use \\
var landuse = ee.Image("projects/ksfi-app/assets/SA_LandUse")
                .clip(ROI.geometry());
                
// Land use weighted layer
var lu_weighted = weights.lu_weight(landuse).clip(ROI);

//Map.addLayer(lu_weighted)

// Soils \\
// Import soil layer, classify to main soil units and rasterize
var soils_feat = ee.FeatureCollection("projects/ksfi-app/assets/ZA_SOILS");

// Convert soil classes to numeric
var distinctClassNames = ee.List(soils_feat.aggregate_array('SOIL')).distinct();
var distinctClassCodes = ee.List.sequence(1, distinctClassNames.size());
var soil_remap = soils_feat.remap(distinctClassNames, distinctClassCodes, "SOIL");

// Remap subcodes to main soil classes
var codes_soils = ee.List(
  [49,1,17,48,18,   // 1: Acrisols
  19,20,21,22,12,   // 2: Arenosols
  13,               // 3: Calcisol
  23,24,25,26,2,    // 4: Cambisols
  27,               // 5: Coarse deposits
  3,53,4,           // 6: Ferrasols
  5,                // 7: Fluvisols
  28,29,            // 8: Gleysols
  46,6,30,11,52,    // 9: Kastanozems
  31,50,7,32,33,34, // 10: Leptosols
  8,35,             // 11: Lixisols
  14,               // 12: Luvisols
  36,47,            // 13: Nitisols
  51,37,            // 14: Phaeozems
  38,39,            // 15: Planosols
  40,               // 16: Plinthosols
  41,9,15,          // 17: Podzols
  10,               // 18: Regosols
  42,43,            // 19: Solonchaks
  16,               // 20: Solonetz
  44,45,            // 21: Vertisols
  54]);             // 22: Water (weigthed 0)
var codes_main = ee.List([1,1,1,1,2,2,2,2,2,2,3,4,4,4,4,4,5,6,6,6,7,8,8,9,9,9,9,9,10,10,10,10,10,10,11,11,12,
      13,13,14,14,15,15,16,17,17,17,18,19,19,20,21,21,22]);
var soil_remapMain = soil_remap.remap(codes_soils, codes_main, "SOIL");

// Rasterize
var soil_rst = soil_remapMain.reduceToImage({
  properties: ['SOIL'],
  reducer: ee.Reducer.first()
});

// Processing: clip raster, retrieve soils in ROI, index for corresponding 
var soil_rst_clip = soil_rst.clip(ROI);

var reduction = soil_rst_clip.reduceRegion({
  reducer:ee.Reducer.frequencyHistogram(), 
  geometry:ROI, 
  scale: 30});

// Remove all the unnecessary reducer output structure and make a list of values unique to the ROI.
var values = ee.Dictionary(reduction.get(soil_rst_clip.bandNames().get(0)))
    .keys()
    .map(ee.Number.parse);

// Calculate weights based on values and remap raster to weighted layer
var weights_calc = weights.soil_weight(values);
var soil_weighted = soil_rst_clip.remap(values, weights_calc);

// Water \\
// Water polygons
var SA_WaterPoly_clip = ee.FeatureCollection("projects/ksfi-app/assets/SA_WaterPoly")
                .filterBounds(ROI.geometry());
// Add property w/ numeric values to convert feature to image
// https://gis.stackexchange.com/questions/347880/rasterize-vector-based-on-list-values-in-google-earth-engine
var distinctClassNamesWP = ee.List(SA_WaterPoly_clip.aggregate_array('natural')).distinct();
var distinctClassCodesWP = ee.List.sequence(1, distinctClassNamesWP.size());
var WP_remap = SA_WaterPoly_clip.remap(distinctClassNamesWP, distinctClassCodesWP, "natural");

// Remap
var waterPoly_rst = WP_remap.reduceToImage({
  properties: ['natural'],
  reducer: ee.Reducer.first()
}).clip(ROI.geometry());

var waterPoly_prox = calc_proximity.calc_proximity(waterPoly_rst);

// Water - lines
var SA_WaterLines = ee.FeatureCollection("projects/ksfi-app/assets/SA_WaterLines")
                .filterBounds(ROI.geometry());
                
// Add property w/ numeric values to convert feature to image
// https://gis.stackexchange.com/questions/347880/rasterize-vector-based-on-list-values-in-google-earth-engine
var distinctClassNames = ee.List(SA_WaterLines.aggregate_array('water')).distinct();
var distinctClassCodes = ee.List.sequence(1, distinctClassNames.size());

// Remap
var WL_remap = SA_WaterLines.remap(distinctClassNames, distinctClassCodes, "water");

var waterLine_rst = WL_remap.reduceToImage({
  properties: ['water'],
  reducer: ee.Reducer.first()
}).clip(ROI.geometry());

var waterLine_prox = calc_proximity.calc_proximity(waterLine_rst);

// Calculate water weighted raster:
var water_weighted = weights.water_weight(waterLine_prox, waterPoly_prox).clip(ROI);

// Map.addLayer(water_weighted, {}, "Water weighted layer")

// Buildings \\

var SA_building = ee.FeatureCollection("projects/ksfi-app/assets/SA_buildings")
                .filterBounds(ROI.geometry());
// Add property w/ numeric values to convert feature to image
// https://gis.stackexchange.com/questions/347880/rasterize-vector-based-on-list-values-in-google-earth-engine
var distinctClassNames = ee.List(SA_building.aggregate_array('building')).distinct();
var distinctClassCodes = ee.List.sequence(1, distinctClassNames.size());

// Remap
var build_remap = SA_building.remap(distinctClassNames, distinctClassCodes, "building");

var building_rst = build_remap.reduceToImage({
  properties: ['building'],
  reducer: ee.Reducer.first()
}).clip(ROI.geometry());

var building_prox = calc_proximity.calc_proximity(building_rst);
// // Calculate weighted layer
var build_weighted = weights.build_weight(building_prox).clip(ROI);

//Map.addLayer(build_weighted, {}, "Building weighted")

// Roads \\

var SA_roads = ee.FeatureCollection("projects/ksfi-app/assets/SA_Roads")
                .filterBounds(ROI.geometry());
// Add property w/ numeric values to convert feature to image
// https://gis.stackexchange.com/questions/347880/rasterize-vector-based-on-list-values-in-google-earth-engine
var distinctClassNames = ee.List(SA_roads.aggregate_array('highway')).distinct();
var distinctClassCodes = ee.List.sequence(1, distinctClassNames.size());

// Remap
var road_remap = SA_roads.remap(distinctClassNames, distinctClassCodes, "highway");

var roads_rast = road_remap.reduceToImage({
  properties: ['highway'],
  reducer: ee.Reducer.first()
}).clip(ROI.geometry());

var road_prox = calc_proximity.calc_proximity(roads_rast);

// Make weighted layer
var road_weighted = weights.road_weight(road_prox).clip(ROI);

//Map.addLayer(road_weighted, {}, "Road weights")

///// Calculate suitability /////

var suitability = weights.suitability(road_weighted, build_weighted, water_weighted, lu_weighted, topo_weighted, soil_weighted );

///// Visualize /////
suitability
  .reduceRegion({
    reducer: ee.Reducer.stdDev().combine(ee.Reducer.mean(), '', true),
    geometry: ROI.geometry(),
    scale: 100,
    maxPixels: 1e9
  })
  .evaluate(function (stats) {
    var stdDev = stats.constant_stdDev;
    var mean = stats.constant_mean;
    var min = mean - stdDev *2 ;
    var max = mean + stdDev *2;
    var visParams = {
      min: min,
      max: max,
      palette: ["A50026","F88E52","FFFFBF","86CB66","006837"]
    };
    Map.addLayer(suitability, visParams, "suitability map");
});

/////// Application

// Button Widget to change ROI

var provinceDD = ui.Select([], 'Select province');
var municipalityDD = ui.Select([], 'Select municipality');
var wardDD = ui.Select([], 'Select ward');

var provinces = ee.FeatureCollection("projects/ksfi-app/assets/SA_admlvl4").distinct('ADM1_EN').sort('ADM1_EN');
var provNames = provinces.aggregate_array('ADM1_EN');
var municipalities = ee.FeatureCollection("projects/ksfi-app/assets/SA_admlvl4");
var ward = ee.FeatureCollection("projects/ksfi-app/assets/SA_admlvl4");

// Automatically update drop-down menus based on administrative unit selection
provNames.evaluate(function(provinces){
  provinceDD.items().reset(provinces);
  provinceDD.setPlaceholder('Select a province');
  provinceDD.onChange(function(prov){
    // once you select a state (onChange) get all counties and fill the dropdown
    municipalityDD.setPlaceholder('Loading...');
    var municipalities = _getMuni(prov);
    municipalities.evaluate(function(muniNames){
      municipalityDD.items().reset(muniNames);
      municipalityDD.setPlaceholder('Select a municipality');
      municipalityDD.onChange(function(subward){
        // once you select a county (onChange) get all sub-counties and fill the dropdown
        wardDD.setPlaceholder('Loading...');
        var ward = _getWard(subward);
        ward.evaluate(function(muniNames){
        wardDD.items().reset(muniNames);
        wardDD.setPlaceholder('Select a ward');
        });
      });
    });
  });
});

// Filter the counties list to retrieve the ones matching the selected state
function _getMuni(prov){
  // Given a state get all counties
  var feat = ee.Feature(provinces.filterMetadata('ADM1_EN', 'equals', prov).first());
  var statefp = ee.String(feat.get('ADM1_EN'));
  var filteredMuni = municipalities.filterMetadata('ADM1_EN', 'equals', statefp)
                         .distinct('ADM3_EN').sort('ADM3_EN', false);
  var filteredMuniNames = filteredMuni.aggregate_array('ADM3_EN');

  return filteredMuniNames.add(prov).reverse();
}


// Filter the sub-counties list to retrieve the ones matching the selected county
function _getWard(muni){
  // Given a state get all counties
  var feat = ee.Algorithms.If(
             ee.Number(municipalities.filterMetadata('ADM3_EN', 'equals', muni).size()).gt(0),
             ee.Feature(municipalities.filterMetadata('ADM3_EN', 'equals', muni).first()),
             ee.Feature(provinces.filterMetadata('ADM1_EN', 'equals', muni).first()).set({'ADM3_EN': muni})
             );
  var statefp = ee.String(ee.Feature(feat).get('ADM3_EN'));
  var filteredWard = ward.filterMetadata('ADM3_EN', 'equals', statefp)
                         .filterMetadata('ADM1_EN', 'equals', provinceDD.getValue())
                         .distinct('ADM4_EN').sort('ADM4_EN', false);
  var filteredWardNames = filteredWard.aggregate_array('ADM4_EN');

  return filteredWardNames.add(muni).reverse();
}
// Add link to git
var git_label = ui.Label({value: 'Background & data️', style: {fontWeight: 'bold'}})
.setUrl('https://git.wur.nl/verho143/ksfi-farm-hub-upscaling');

// Add Button Widget to finalize drop-down menu selections.
var add = ui.Button('Display Area of Interest');

// Add all buttons to the panel
var panel = ui.Panel([git_label,
                      provinceDD,
                      municipalityDD,
                      wardDD,
                      add],
                      ui.Panel.Layout.flow('vertical'));

panel.style().set({width: '325px'});

ui.root.add(panel);

var title_label = ui.Label({
  value: "Eatern Cape, Buffalo City, Ward 032",
  style: {
    fontWeight: 'bold',
    fontSize: '18px',
    margin: '0 0 4px 0',
    padding: '2'
    }
})
Map.add(title_label);
// set position of panel
var legend = ui.Panel({
  style: {
    position: 'bottom-left',
    padding: '8px 15px'
  }
});
 
// Create legend title (https://mygeoblog.com/2016/12/09/add-a-legend-to-to-your-gee-map/)
var legendTitle = ui.Label({
  value: 'Suitability',
  style: {
    fontWeight: 'bold',
    fontSize: '18px',
    margin: '0 0 4px 0',
    padding: '0'
    }
});
// Add the title to the panel
legend.add(legendTitle);
// Creates and styles 1 row of the legend.
var makeRow = function(color, name) {
 
      // Create the label that is actually the colored box.
      var colorBox = ui.Label({
        style: {
          backgroundColor: '#' + color,
          // Use padding to give the box height and width.
          padding: '8px',
          margin: '0 0 4px 0'
        }
      });
 
      // Create the label filled with the description text.
      var description = ui.Label({
        value: name,
        style: {margin: '0 0 4px 6px'}
      });
 
      // return the panel
      return ui.Panel({
        widgets: [colorBox, description],
        layout: ui.Panel.Layout.Flow('horizontal')
      });
};
 
//  Palette with the colors
var palette =["A50026","F88E52","FFFFBF","86CB66","006837"];
// name of the legend
var names = ['Very low','Low','Medium', 'High', 'Very high'];
// Add color and and names
for (var i = 0; i < 5; i++) {
  legend.add(makeRow(palette[i], names[i]));
  }  
// add legend to map (alternatively you can also print the legend to the console)
Map.add(legend);


// Trigger button to generate new area
add.onClick(function(){
  // Clean-up the map and panel displays when new information is requested
  Map.clear();
  Map.add(legend);

  var ward = wardDD.getValue();
  var municipalities = municipalityDD.getValue();
  var provinces = provinceDD.getValue();
  var province = ee.FeatureCollection("projects/ksfi-app/assets/SA_admlvl4")
                    .filter(ee.Filter.eq('ADM1_EN', provinces));
  var municipality = province.filter(ee.Filter.eq("ADM3_EN", municipalities));
  var ROI = municipality.filter(ee.Filter.eq("ADM4_EN", ward));
  Map.centerObject(ROI);
  var title_label = ui.Label({
    value: (provinces + ', ' + municipalities + ', Ward ' + ward),
    style: {
      fontWeight: 'bold',
      fontSize: '18px',
      margin: '0 0 4px 0',
      padding: '2'
      }
    });
  Map.add(title_label);
  // Topography \\
  // Slope
  var slope = ee.Image("projects/ksfi-app/assets/SA_slope")
                  .clip(ROI.geometry());
  // Aspect
  var aspect = ee.Image("projects/ksfi-app/assets/SA_aspect")
                  .clip(ROI.geometry());
                  
  // Topo weighted layer
  var topo_weighted = weights.topo_weight(slope, aspect).clip(ROI);
  // Map.addLayer(topo_weighted)
                  
  // Land use \\
  var landuse = ee.Image("projects/ksfi-app/assets/SA_LandUse")
                  .clip(ROI.geometry());
                  
  // Land use weighted layer
  var lu_weighted = weights.lu_weight(landuse).clip(ROI);
  // Map.addLayer(lu_weighted)
  
  // Soils
  // Import soil layer, classify to main soil units and rasterize
  var soils_feat = ee.FeatureCollection("projects/ksfi-app/assets/ZA_SOILS");
  
  // Convert soil classes to numeric
  var distinctClassNames = ee.List(soils_feat.aggregate_array('SOIL')).distinct();
  var distinctClassCodes = ee.List.sequence(1, distinctClassNames.size());
  var soil_remap = soils_feat.remap(distinctClassNames, distinctClassCodes, "SOIL");
  
  // Remap subcodes to main soil classes
  var codes_soils = ee.List(
    [49,1,17,48,18,   // 1: Acrisols
    19,20,21,22,12,   // 2: Arenosols
    13,               // 3: Calcisol
    23,24,25,26,2,    // 4: Cambisols
    27,               // 5: Coarse deposits
    3,53,4,           // 6: Ferrasols
    5,                // 7: Fluvisols
    28,29,            // 8: Gleysols
    46,6,30,11,52,    // 9: Kastanozems
    31,50,7,32,33,34, // 10: Leptosols
    8,35,             // 11: Lixisols
    14,               // 12: Luvisols
    36,47,            // 13: Nitisols
    51,37,            // 14: Phaeozems
    38,39,            // 15: Planosols
    40,               // 16: Plinthosols
    41,9,15,          // 17: Podzols
    10,               // 18: Regosols
    42,43,            // 19: Solonchaks
    16,               // 20: Solonetz
    44,45,            // 21: Vertisols
    54]);             // 22: Water (weigthed 0)
  var codes_main = ee.List([1,1,1,1,2,2,2,2,2,2,3,4,4,4,4,4,5,6,6,6,7,8,8,9,9,9,9,9,10,10,10,10,10,10,11,11,12,
        13,13,14,14,15,15,16,17,17,17,18,19,19,20,21,21,22]);
  var soil_remapMain = soil_remap.remap(codes_soils, codes_main, "SOIL");
  
  // Rasterize
  var soil_rst = soil_remapMain.reduceToImage({
    properties: ['SOIL'],
    reducer: ee.Reducer.first()
  });
  
  // Processing: clip raster, retrieve soils in ROI, index for corresponding 
  var soil_rst_clip = soil_rst.clip(ROI);
  
  var reduction = soil_rst_clip.reduceRegion({
    reducer:ee.Reducer.frequencyHistogram(), 
    geometry:ROI, 
    scale: 30});
  
  // Remove all the unnecessary reducer output structure and make a list of values unique to the ROI.
  var values = ee.Dictionary(reduction.get(soil_rst_clip.bandNames().get(0)))
      .keys()
      .map(ee.Number.parse);
  
  // Calculate weights based on values and remap raster to weighted layer
  var weights_calc = weights.soil_weight(values);
  var soil_weighted = soil_rst_clip.remap(values, weights_calc);
  // Map.addLayer(soil_weighted, {}, 'soil')
  
  // Water \\
  // Water polygons
  var SA_WaterPoly_clip = ee.FeatureCollection("projects/ksfi-app/assets/SA_WaterPoly")
                  .filterBounds(ROI.geometry());
  // Add property w/ numeric values to convert feature to image
  // https://gis.stackexchange.com/questions/347880/rasterize-vector-based-on-list-values-in-google-earth-engine
  var distinctClassNamesWP = ee.List(SA_WaterPoly_clip.aggregate_array('natural')).distinct();
  var distinctClassCodesWP = ee.List.sequence(1, distinctClassNamesWP.size());

  // Remap
  var WP_remap = SA_WaterPoly_clip.remap(distinctClassNamesWP, distinctClassCodesWP, "natural");
  
  // Apply the addNameCode function to the feature collection.
  // var waterpoly = SA_WaterPoly_clip.map(addNameCodeWP);
  
  var waterPoly_rst = WP_remap.reduceToImage({
    properties: ['natural'],
    reducer: ee.Reducer.first()
  }).clip(ROI.geometry());
  // Map.addLayer(waterPoly_rst)
  var waterPoly_prox = calc_proximity.calc_proximity(waterPoly_rst).unmask(2222);
  // Map.addLayer(waterPoly_prox, {}, 'waterpoly prox')
  // Water - lines
  var SA_WaterLines = ee.FeatureCollection("projects/ksfi-app/assets/SA_WaterLines")
                  .filterBounds(ROI.geometry());
                  
  // Add property w/ numeric values to convert feature to image
  // https://gis.stackexchange.com/questions/347880/rasterize-vector-based-on-list-values-in-google-earth-engine
  var distinctClassNames = ee.List(SA_WaterLines.aggregate_array('water')).distinct();
  var distinctClassCodes = ee.List.sequence(1, distinctClassNames.size());

  //Remap
  var WL_remap = SA_WaterLines.remap(distinctClassNames, distinctClassCodes, "water");
  
  var waterLine_rst = WL_remap.reduceToImage({
    properties: ['water'],
    reducer: ee.Reducer.first()
  }).clip(ROI.geometry());
  
  var waterLine_prox = calc_proximity.calc_proximity(waterLine_rst).unmask(2222);
  // Map.addLayer(waterLine_prox, {}, 'water line prox')
  // Calculate water weighted raster:
  
  var water_weighted = weights.water_weight(waterLine_prox, waterPoly_prox).clip(ROI);
  // Map.addLayer(water_weighted, {}, "Water weighted layer")
  
  // Buildings \\
  
  var SA_building = ee.FeatureCollection("projects/ksfi-app/assets/SA_buildings")
                  .filterBounds(ROI.geometry());
  // Add property w/ numeric values to convert feature to image
  // https://gis.stackexchange.com/questions/347880/rasterize-vector-based-on-list-values-in-google-earth-engine
  var distinctClassNames = ee.List(SA_building.aggregate_array('building')).distinct();
  var distinctClassCodes = ee.List.sequence(1, distinctClassNames.size());

  // Remap
  var build_remap = SA_building.remap(distinctClassNames, distinctClassCodes, "building");
  
  var building_rst = build_remap.reduceToImage({
    properties: ['building'],
    reducer: ee.Reducer.first()
  }).clip(ROI.geometry());
  
  var building_prox = calc_proximity.calc_proximity(building_rst);
  // // Calculate weighted layer
  var build_weighted = weights.build_weight(building_prox).clip(ROI);
  // Map.addLayer(build_weighted, {}, "Building weighted")
  
  // Roads \\
  
  var SA_roads = ee.FeatureCollection("projects/ksfi-app/assets/SA_Roads")
                  .filterBounds(ROI.geometry());
  // Add property w/ numeric values to convert feature to image
  // https://gis.stackexchange.com/questions/347880/rasterize-vector-based-on-list-values-in-google-earth-engine
  var distinctClassNames = ee.List(SA_roads.aggregate_array('highway')).distinct();
  var distinctClassCodes = ee.List.sequence(1, distinctClassNames.size());

  // Remap
  var road_remap = SA_roads.remap(distinctClassNames, distinctClassCodes, "highway");
  
  var roads_rast = road_remap.reduceToImage({
    properties: ['highway'],
    reducer: ee.Reducer.first()
  }).clip(ROI.geometry());
  
  var road_prox = calc_proximity.calc_proximity(roads_rast);
  
  // Make weighted layer
  var road_weighted = weights.road_weight(road_prox).clip(ROI);
  // Map.addLayer(road_weighted, {}, "Road weights")
  
  ///// Calculate suitability /////

  var suitability = weights.suitability(road_weighted, build_weighted, water_weighted, lu_weighted, topo_weighted, soil_weighted );
  // Reduce resolution:

  /// Visualize
  suitability
    .reduceRegion({
      reducer: ee.Reducer.stdDev().combine(ee.Reducer.mean(), '', true),
      geometry: ROI.geometry(),
      scale: 100,
      maxPixels: 1e9
    })
    .evaluate(function (stats) {
      var stdDev = stats.constant_stdDev;
      var mean = stats.constant_mean;
      var min = mean - stdDev *2;
      var max = mean + stdDev *2;
      var visParams = {
        min: min,
        max: max,
        palette: ["A50026","F88E52","FFFFBF","86CB66","006837"]
      };

      Map.addLayer(suitability, visParams, "suitability map");
});
});

/*
Visualize the generated suitability raster.
- think of palette & legend (> soilwatch has function for legend: useable?)
- how can users of app retrieve map? (.getDownloadURL?/ a button?)
*/
// print(suitability)
// var chart =
//     ui.Chart.image.histogram({image: suitability, region: ROI, scale: 100})
// print(chart)
// Export.image.toDrive({
//   image: suitability,
//   description: 'suitability',
  
//   scale: 50,
//   maxPixels: 1e13
// });

