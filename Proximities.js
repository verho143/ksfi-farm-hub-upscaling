// distance calculation
exports.calc_proximity = function(rst){
  var rast = rst.unmask(0)
  var dist = rast.distance({kernel: ee.Kernel.euclidean({radius:10000, units: "meters"}), skipMasked:false})
//  .clip(ROI)
  .rename('distance')
  return dist
}

