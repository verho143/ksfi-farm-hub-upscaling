// Functions to reclassify of weighted layers, or retrieve the weight and apply to layers.
// TO DO: include documentation about how the weights are obtained

// Roads proximity

exports.road_weight = function(img){
  // img gives proximity to roads, in meters
  var img_weight = ee.Image(0)
      .where(img.lte(500), 0.45)
      .where(img.gt(500).and(img.lte(1000)), 0.25)
      .where(img.gt(1000).and(img.lte(1500)), 0.15)
      .where(img.gt(1500).and(img.lte(2000)), 0.1)
      .where(img.gt(2000), 0.05);
  return img_weight;
}

// Building proximity

exports.build_weight = function(img){
  // img gives proximity to buildings/villages, in meters
  var img_weight = ee.Image(0)
      .where(img.lte(250), 0.45)
      .where(img.gt(250).and(img.lte(500)), 0.25)
      .where(img.gt(500).and(img.lte(750)), 0.15)
      .where(img.gt(750).and(img.lte(1000)), 0.1)
      .where(img.gt(1000), 0.05);
   return img_weight;
}

// Water proximity

exports.water_weight = function(img_lines, img_poly){
  // img gives proximity to water lines or water polygons (bodies), in meters
  var img_weightLine = ee.Image(0)
      .where(img_lines.lte(100), 0.49)
      .where(img_lines.gt(100).and(img_lines.lte(250)), 0.26)
      .where(img_lines.gt(250).and(img_lines.lte(500)), 0.15)
      .where(img_lines.gt(500), 0.1);
  var img_weightPoly = ee.Image(1)
      .where(img_poly.lte(100), 0.49)
      .where(img_poly.gt(100).and(img_poly.lte(250)), 0.26)
      .where(img_poly.gt(250).and(img_poly.lte(500)), 0.15)
      .where(img_poly.gt(500), 0.1);
  var img_weight = img_weightLine.multiply(0.5).add(img_weightPoly.multiply(0.5));
  return img_weight;
}

// Soils

var dict = ee.Dictionary({
  '1.0': [1.00,0.25,0.20,0.20,5.00,0.25,0.25,0.33,0.25,1.00,1.00,0.14,0.14,0.14,5.00,5.00,1.00,0.25,5.00,5.00,0.25,0.00],
  '2.0': [4.00,1.00,0.20,0.20,6.00,1.00,1.00,2.00,1.00,4.00,4.00,0.17,0.17,0.17,6.00,6.00,4.00,0.50,6.00,6.00,1.00,0.00],
  '3.0': [5.00,4.00,1.00,1.00,7.00,4.00,4.00,5.00,4.00,6.00,6.00,0.20,0.20,0.20,7.00,7.00,5.00,4.00,7.00,7.00,4.00,0.00],
  '4.0': [5.00,5.00,1.00,1.00,7.00,4.00,4.00,5.00,4.00,6.00,6.00,0.20,0.20,0.20,7.00,7.00,5.00,4.00,7.00,7.00,4.00,0.00],
  '5.0': [0.20,0.17,0.14,0.14,1.00,0.17,0.17,0.17,0.17,0.20,0.20,0.11,0.11,0.11,1.00,1.00,0.20,0.17,1.00,1.00,0.17,0.00],
  '6.0': [4.00,1.00,0.25,0.25,6.00,1.00,1.00,2.00,1.00,4.00,4.00,0.17,0.17,0.17,6.00,6.00,4.00,1.00,6.00,6.00,1.00,0.00],
  '7.0': [4.00,1.00,0.25,0.25,6.00,1.00,1.00,2.00,1.00,4.00,4.00,0.17,0.17,0.17,6.00,6.00,4.00,1.00,6.00,6.00,1.00,0.00],
  '8.0': [3.00,0.50,0.20,0.20,6.00,0.50,0.50,1.00,0.50,4.00,4.00,0.17,0.17,0.17,6.00,6.00,4.00,0.50,6.00,6.00,0.50,0.00],
  '9.0': [4.00,1.00,0.25,0.25,6.00,1.00,1.00,2.00,1.00,4.00,4.00,0.17,0.17,0.17,6.00,6.00,4.00,1.00,6.00,6.00,1.00,0.00],
  '10.0': [1.00,0.25,0.17,0.17,5.00,0.25,0.25,0.25,0.25,1.00,1.00,0.14,0.14,0.14,5.00,5.00,1.00,0.25,5.00,5.00,0.25,0.00],
  '11.0': [1.00,0.25,0.17,0.17,5.00,0.25,0.25,0.25,0.25,1.00,1.00,0.14,0.14,0.14,5.00,5.00,1.00,0.25,5.00,5.00,0.25,0.00],
  '12.0': [7.00,6.00,5.00,5.00,9.00,6.00,6.00,6.00,6.00,7.00,7.00,1.00,1.00,1.00,9.00,9.00,6.00,4.00,9.00,9.00,4.00,0.00],
  '13.0': [7.00,6.00,5.00,5.00,9.00,6.00,6.00,6.00,6.00,7.00,7.00,1.00,1.00,1.00,9.00,9.00,6.00,4.00,9.00,9.00,4.00,0.00],
  '14.0': [7.00,6.00,5.00,5.00,9.00,6.00,6.00,6.00,6.00,7.00,7.00,1.00,1.00,1.00,9.00,9.00,6.00,4.00,9.00,9.00,4.00,0.00],
  '15.0': [0.20,0.17,0.14,0.14,1.00,0.17,0.17,0.17,0.17,0.20,0.20,0.11,0.11,0.11,1.00,1.00,0.20,0.17,1.00,1.00,0.17,0.00],
  '16.0': [0.20,0.17,0.14,0.14,1.00,0.17,0.17,0.17,0.17,0.20,0.20,0.11,0.11,0.11,1.00,1.00,0.20,0.17,1.00,1.00,0.17,0.00],
  '17.0': [1.00,0.25,0.20,0.20,5.00,0.25,0.25,0.25,0.25,1.00,1.00,0.17,0.17,0.17,5.00,5.00,1.00,0.25,5.00,5.00,0.25,0.00],
  '18.0': [4.00,2.00,0.25,0.25,6.00,1.00,1.00,2.00,1.00,4.00,4.00,0.25,0.25,0.25,6.00,6.00,4.00,1.00,6.00,6.00,1.00,0.00],
  '19.0': [0.20,0.17,0.14,0.14,1.00,0.17,0.17,0.17,0.17,0.20,0.20,0.11,0.11,0.11,1.00,1.00,0.20,0.17,1.00,1.00,0.17,0.00],
  '20.0': [0.20,0.17,0.14,0.14,1.00,0.17,0.17,0.17,0.17,0.20,0.20,0.11,0.11,0.11,1.00,1.00,0.20,0.17,1.00,1.00,0.17,0.00],
  '21.0': [4.00,1.00,0.25,0.25,6.00,1.00,1.00,2.00,1.00,4.00,4.00,0.25,0.25,0.25,6.00,6.00,4.00,1.00,6.00,6.00,1.00,0.00],
  '22.0': [0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00]
  })
  
exports.soil_weight = function(lst){
  var index = ee.List.sequence(0, lst.length().subtract(1))
  var index_codes = ee.Array(lst).subtract(1).toList()

  var string = index.map(function(i){return(ee.String(lst.get(i)))});

  var weights = string.map(
    function(i){
      var matrix = dict.get(i)
      var vals = index_codes.map(function(j){return(ee.List(matrix).get(j))})
      var sum = vals.reduce(ee.Reducer.sum())
    return sum
    })
  
  var weights_f = ee.Array(weights).divide(weights.reduce(ee.Reducer.sum())).toList()
  return weights_f
}


// Land use
// TO DO: FIX THAT
exports.lu_weight = function(img){
  // img gives landuse categorical data, reclassified and weighted.
  var img_weight = ee.Image(0)
      .where(img.eq(12).or(img.eq(13)).or(img.eq(8)).or(img.eq(9)).or(img.eq(10)).or(img.eq(11)).or(img.eq(42)).or(img.eq(43)).or(img.eq(44)).or(img.eq(45)).or(img.eq(46)).or(img.eq(73)).or(img.eq(41)), 0.45) // Grasslands, Shrublands, Fallow & abandoned field,  subsistence/small-scale annual crops
      .where(img.eq(22).or(img.eq(23)).or(img.eq(24)), 0.2) // Wetlands
      .where(img.gte(25).and(img.lte(31)), 0.2) // Barren
      .where(img.gte(1).and(img.lte(7)), 0.1) // Forest
      ;
   return img_weight;
}

// Soil erosion

// Topography

exports.topo_weight = function(slope, aspect){
  // slope gives the slope of the area, in degrees.
  //Aspect gives orientation of the slopes, in degrees (where 0=N, 90=E, 180=S, 270=W)
  var slope_weight = ee.Image(0)
      .where(slope.lte(2), 0.45)
      .where(slope.gt(2).and(slope.lte(6)), 0.25)
      .where(slope.gt(6).and(slope.lte(15)), 0.2)
      .where(slope.gt(15), 0.1);
  var aspect_weight = ee.Image(0)
      .where(aspect.lte(100), 0.49)
      .where(aspect.gt(100).and(aspect.lte(250)), 0.26)
      .where(aspect.gt(250).and(aspect.lte(500)), 0.15)
      .where(aspect.gt(500), 0.1);
  var img_weight = slope_weight.multiply(0.7).add(aspect_weight.multiply(0.3));
  return img_weight;
}

// Calculate map suitability

exports.suitability = function(road, build, water, lu, topo, soil){
  var suit = ee.Image(0).expression(
    '0.012 * road + 0.115 * build + 0.273 * water + 0.150 * lu + 0.221 * topo + 0.230 * soil', {
      'road': road.select('constant'),
      'build': build.select('constant'),
      'water': water.select('constant'),
      'lu': lu.select('constant'),
      'topo': topo.select('constant'),
      'soil': soil.select('remapped')
    });
  return suit;
};
