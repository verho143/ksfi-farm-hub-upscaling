
///// Obtain DEM, clip to SA /////

var dataset = ee.Image('NASA/NASADEM_HGT/001');
var dem = dataset.select('elevation').clip(sa);

Map.addLayer(dem, {min: 7.374791858657659, max: 1948.2461330844812}, " Digital elevation model")
 
Export.image.toAsset({
  image: dem,
  description: 'SA_DEM',
  assetId: 'SA_DEM',
  scale: 30,
  maxPixels: 1e13
});


///// Obtain slope, export to asset /////
var slope = ee.Terrain.slope(dem)

Map.addLayer(slope, {min:0.005098934751003981, max: 0.5115745663642883}, "Slope")


Export.image.toAsset({
  image: slope,
  description: 'SA_slope',
  assetId: 'SA_slope',
  scale: 30,
  maxPixels: 1e13
});


///// Obtain aspect, export to asset /////
var aspect = ee.Terrain.aspect(dem);
Map.addLayer(aspect, {}, "Aspect")


Export.image.toAsset({
  image: aspect,
  description: 'SA_aspect',
  assetId: 'SA_aspect',
  scale: 30,
  maxPixels: 1e13
});


